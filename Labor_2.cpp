
//������⥪� ����� - �뢮��
#include <iostream>
//������⥪� ��� �㭪樨 setprecision(),-��।���� �筮��� �뢮�� ���
#include <iomanip>
#include <stdio.h>


// ������ୠ� ࠡ�� 2.

int main() {
int prog;
// ������ ����� ��������� �� 1 - 5.
std::cout << "\n\n\t������ ����� �ணࠬ�� (1, 2, 3, 4 ��� 5) � ������ enter\n\n";
std::cin>>prog;

if(prog == 1){
// �ணࠬ�� 1.
// �뢮� ������ � �ᯮ�짮������ ����䨪��஢ �ଠ�஢����.

std::cout <<std::setw(60)<<std::right<<"�����\n\n\n"<<std::endl;
std::cout <<std::setw(100)<<std::right<<"�������: ���. ��ࠧ������"<<std::endl;
std::cout <<std::setw(84)<<std::right<<"��㯯�: 310"<<std::endl;
std::cout <<std::setw(97)<<std::right<<"��㤥��: ���⨭ �����\n\n\n"<<std::endl;
std::cout <<std::setw(65)<<std::right<<"���� 2023�."<<std::endl;
std::cout <<"\n\n\t��� ��室� �� �ணࠬ�� ������ enter";

}
else if(prog == 2){
        // �ணࠬ�� 2.
        std::cout <<"\n\t���������� ������ ������� ����� ������\n";

        std::cout << "  char:\t\t" << sizeof(char) << " bytes" << std::endl;
        std::cout << "  wchar_t:\t" << sizeof(wchar_t) << " bytes" << std::endl;
        std::cout << "  char16_t:\t" << sizeof(char16_t) << " bytes" << std::endl;
        std::cout << "  char32_t:\t" << sizeof(char32_t) << " bytes" << std::endl;
        std::cout << "  short:\t\t" << sizeof(short) << " bytes" << std::endl;
        std::cout << "  int:\t\t" << sizeof(int) << " bytes" << std::endl;
        std::cout << "  long:\t\t" << sizeof(long) << " bytes" << std::endl;
        std::cout << "  long long:\t" << sizeof(long long) << " bytes" << std::endl;
        std::cout << "  float:\t\t" << sizeof(float) << " bytes" << std::endl;
        std::cout << "  double:\t\t" << sizeof(double) << " bytes" << std::endl;
        std::cout << "  long double:\t" << sizeof(long double) << " bytes" << std::endl;

        /* ������� ����䨪����: signed (��६����� � ������⥫�묨 � ����⥫�묨 �᫠��)
        � unsigned (��६����� ⮫쪮 � ������⥫�묨 �᫠��).
           ����䨪���� ��⨬���樨 ࠧ���:  short � long (�� < 16� � �� < 32x ���)  */

        std::cout << "\n\t���������� ������ ������� ����� ������  � ��������������\n";

        std::cout << "  bool              : "<< sizeof(bool) << " bytes" << std::endl;
        std::cout << "  signed char       : " << sizeof(signed char) << " bytes" << std::endl;
        std::cout << "  unsigned char     : " << sizeof(unsigned char) << " bytes" << std::endl;
        std::cout << "  long double       : " << sizeof(long double) << " bytes" << std::endl;
        std::cout << "  long int          : " << sizeof(long int) << " bytes" << std::endl;
        std::cout << "  signed short      : " << sizeof(signed short) << " bytes" << std::endl;
        std::cout << "  unsigned short    : " << sizeof(unsigned short) << " bytes" << std::endl;
        std::cout << "  unsigned int      : " << sizeof(unsigned int) << " bytes" << std::endl;
        std::cout << "  signed int        : " << sizeof(signed int) << " bytes" << std::endl;
        std::cout << "  unsigned long int : " << sizeof(unsigned long int) << " bytes" << std::endl;
}
else if(prog == 3){
        // �ணࠬ�� 3.
        std::cout << "  �������� �।�� ��䬥��᪮�� �� ��� �ᥫ.\n";

        float a, b, c;
        std::cout << "  ����� ��ࢮ� �᫮ � ������ enter\n";
        std::cin >> a;
        std::cout << "  ����� ��஥ �᫮ � ������ enter\n";
        std::cin >> b;
        std::cout << "  ����� ���� �᫮ � ������ enter\n";
        std::cin >> c;
        float d;
        d = (a + b + c) / 3;
        std::cout << "  १����:  " << d;
}
else if(prog == 4){
        // �ணࠬ�� 4.
        std::cout << "\n  ���᫥��� ᪮��� ��אַ��������� ��������.\n";

        float e, f, g;
        std::cout << "  ����� ����ﭨ� � ��. � ������ enter\n";
        std::cin >> e;
        std::cout << "  ����� �६� � ��� � ������ enter\n";
        std::cin >> f;
        g = (e / f);
        std::cout << "  १����:  " << g << "��/��\n";

        std::cout << "  \n\n���᫥��� �᪮७��.\n";

        float a1, b1, c1, d1;
        std::cout << "  ����� ��砫��� ᪮���� � �/� � ������ enter\n";
        std::cin >> a1;
        std::cout << "  ����� ������� ᪮���� � �/� � ������ enter\n";
        std::cin >> b1;
        std::cout << "  ����� �६� � ᥪ㭤�� � ������ enter\n";
        std::cin >> c1;
        d1 = (b1 - a1) / c1;
        std::cout << "  १����:  " << d1 << "�/ᥪ ^2\n";

        std::cout << "  \n\n���᫥��� ࠤ��� ��㣠.\n";

        float a2, b2;
        std::cout << "  ����� ����� ���㦭��� � �. � ������ enter\n";
        std::cin >> a2;
        b2 = a2 / 6.28;
        std::cout << "  १����:  " << b2 << "�.\n";
}
else if(prog == 5){
        // �ணࠬ�� 5.
//������ �筮��� 1 � ������� �㭪樨 setprecision(1)
std::cout<<std::setprecision(1);

//�ᯮ��㥬 �� ⨯� ������ � ������饩 �窮�, - �� ����砫쭠� �筮��� ����� ����������
//��ᢠ����� ��६���� �᫮�� ���祭��
double ex(34.50);
long double ex1(0.004000);
float ex2(123.005);
float ex3(146000);

//�뢮��� �� ���᮫� ���� � ��ᯮ���樠�쭮� �ଥ
std::cout << ex << std::endl << ex1 << std::endl << ex2 << std::endl << ex3;

/*
 * �������⢮ ������ ���(�� "�"): - 祬 �� �����, ⥬ ���� �������� �筮���
 * ex -  1
 * ex1 - 4
 * ex2 - 1
 * ex3 - 1
*/

}
return 0;
}

